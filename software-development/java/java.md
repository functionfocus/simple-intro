#Java
## Java is a compiled language
A compiled language has a step between the author writing the code and the code being executed. That step involves a software program reading the code and converting it into optimized instructions for the computer to execute. 

## Java programs run against the Java Virtual Machine
The Java Virtual Machine, or JVM, is a software program that actually executes compiled Java code. In most cases, this step is reasonably transparent to the developer.  The Java developer does need to know what versions of the JVM are available on a computer when writing a Java program, but this is mostly because new features in the language are only available on newer versions of the JVM. 

## Java is Strictly Typed
In Java, all data has some specific type (or potentially a type hierarchy).  Once assigned, variables cannot change type.  Unlike some languages which allow a integer variable to become a String, and later an array, Java does not allow this. This feature means that potential errors in variable type are identified automatically while writing the code and never get executed. 

## Java is Object Oriented
When writing in programs in Java, you will be creating Classes which are containers for data (member variables) and functionality (methods). Object oriented programming can be a great way to solve a a diverse group of problems, especially complex problems typically found in real life.  This capacity to solve complex problems does come with some complexity of its own, which is why so many small Java programs found in programming tutorials seem to cumbersome and verbose. 

## Java code looks like this
An trivial Java program might look like this (see the code below).  This program would be stored in the file:  {some arbitrary folder location}/com/functionfocus/simpleintro/java/OperatingSystemUser.java

```java
package com.functionfocus.simpleintro.java
/**
 * A class to read the name of the user currently logged-in to the Operating System
 */
public class OperatingSystemUser {
    
    private String username;

    /**
     * The JVM constant for the username system property
     */
    protected static final String SYSTEM_PROPERTY_USER_NAME = "user.name";


    public OperatingSystemUser(){

    }

    /**
     * This special method allows the program to be executed.
     * When it is executed, the program will:
     * <ul>
     *     <li>instantiate a new instance of OperatingSystemUser,</li>
     *     <li>read the value of the user.name constant, and </li>
     *     <li>print this value to the console. </li>
     * </ul>
     */
    public static void main(String[] args){
        OperatingSystemUser osUser = new OperatingSystemUser();
        System.out.println("Current System User is " + osUser.getUsername());
    }

    /**
     * Read the value of the OS constant for user.name
     */
    private String readUsernameFromOperatingSystem(){
        return System.getProperty(SYSTEM_PROPERTY_USER_NAME);
    }

    /**
     * Returns the value of the instance's username field. 
     * If this value hasn't been set yet, call the readUsernameFromOperatingSystem 
     * method to read this value before returning it.
     */
    public String getUsername(){
        if(username == null){
            this.setUsername(readUsernameFromOperatingSystem());
        }
        return username;
    }

    public String setUsername(String username){
        this.username = username;
    }

}
```

## Java programs are generally composed of many classes
The program example above is a functional, if trivial program which consists of a single class. For sure, it's possible to write useful Java programs that consist of only a single class- but this is pretty unusual in practice.  The majority of Java programs that you'll write will be composed of a large number of classes, each of which (should) be fairly limited in scope.

## Java classes are split into "packages"


