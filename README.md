# simple-intro
## What's the Goal?
This project contains a collection of resources to aid the interested individual in
learning the basics of a number of different topics.  The goal isn't to cover 
the entirety of every topic in a subject, nor is it even to be an in-depth introduction.
Rather, this project aims to bridge the gap from zero to one for a wide survey 
of any topic.

## Who's the Audience?
There are already a lot of tutorials in many topics aimed at individuals who are 
motivated to become skilled practicioners in some technical topic.  If you want to
get a role as a developer, then start there!  Or start here, if you want. This content
doesn't assume that you are on the track to become a developer. Rather, this content
is written for individuals in the software development ecosystem who are around
developers, who want to better understand how software development works, but who
aren't (necessarily) going to develop software themselves.  This content is also
well-suited to developers who have expertise in one tech stack but who want a brief
survey in a different technology.

## What's the Status of this Project?
This is a new project, started out of a desire to see this content exist, and is 
still early in the content creation process.  